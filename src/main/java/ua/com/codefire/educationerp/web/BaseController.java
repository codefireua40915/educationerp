/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.educationerp.web;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author human
 */
@Controller
@RequestMapping("/")
public class BaseController {
    
    @ResponseStatus(HttpStatus.PERMANENT_REDIRECT)
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String getRoot() {
        return "redirect:/index";
    }
    
    @RequestMapping(path = "/index", method = RequestMethod.GET)
    public String getIndex() {
        return "index";
    }
}
